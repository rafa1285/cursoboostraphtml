//Activa la funcion tooltip y los popovers.Data - toggle es lo que lo va a relacionar Aplicamos sobre el boton contacto para registrarse diferentes efectos
//al pulsarlo
$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 3000
    });
    $('#contacto').on('show.bs.modal', function (e) {
        console.log('el modal se esta mostrando');

        $('#contactoBtn').removeClass('btn btn-successbtn-danger');
        $('#contactoBtn').addClass('btn btn-danger');
        $('#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('el modal se mostro');
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('el modal se esta ocultando');
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('el modal se oculto');
    });
});